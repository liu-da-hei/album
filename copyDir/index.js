/*
 *拷贝文件及文件夹
*/
const fs = require("fs");

function cpDir(oldDirPath, newDirPath) {
  return new Promise(function(resolve, reject) {
    if (!fs.existsSync(oldDirPath)) {
      reject();
    } else {
      copyHandle(oldDirPath, newDirPath);
      resolve();
    }
  });
}
function copyHandle(oldDirPath, newDirPath) {
  fs.copyFile(oldDirPath, newDirPath, function(err) {
    if (err) throw err;
    fs.stat(oldDirPath, function(err, stats) {
      if (err) throw err;
      if (!stats.isFile()) {
        fs.readdir(oldDirPath, function(err, files) {
          if (err) throw err;
          if (files.length >= 1) {
            files.forEach(file => {
              copyHandle(oldDirPath + "/" + file, newDirPath + "/" + file);
            });
          }
        });
      }
    });
  });
}
//调用
const p = cpDir("oldDir", "newDir");
p.then(() => console.log("拷贝成功")).catch((err) => console.log("拷贝失败"));
