window.onload = function() {
  //导航
  let navIcon = document.querySelector(".navIcon");
  let firstUl = document.querySelector(".myNav > ul");
  navIcon.addEventListener("click", function() {
    firstUl.classList.toggle("open");
  });
  firstUl.addEventListener("click", function(e) {
    if (e.target.nodeName == "A") {
      firstUl.classList.remove("open");
    }
  });
  firstUl.addEventListener("mouseleave", function(e) {
    firstUl.classList.remove("open");
  });
}
