const express = require('express');
const router = express.Router();
const products = require('../models/products');

router.get('/', function(req, res, next) {
    res.render('products', { list: products.creatProducts() });
});

module.exports = router;
